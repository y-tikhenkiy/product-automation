import faker = require("faker");
import { expect } from "chai";
import { OFControls } from "../../page_object/orderFormControls";
import { AuthForm } from "../../page_object/authForm";



describe('surfing at OF', function(){
    before(function(){
        browser.maximizeWindow();
    })
    
    beforeEach(function(){
        browser.url('./');
        browser.pause(500);
        browser.deleteCookies();
            
        //browser.click('#hide-cookie-banner'); //hide GDPR banner
    })
    
    //variables
    let order: OFControls = new OFControls('ew'); // дописал Рудь, как пример для реализации задуманного ключа в класс
    let clientAuth: AuthForm = new AuthForm('ew');

    it("should goto order form page", function(){
        order.goToOrderForm();// clicking by Order Now button at main page by XPath
        // console.log(order.siteKey);
        expect(browser.getTitle()).is.equal('Order Custom Writing at EvolutionWriters.com Right Away!');//verify title for complience
    });

    it("should be visible allert about empty Paper details", function(){
        order.goToOrderForm();
        order.goToCheckout(); //clicking Continue button
        browser.pause(500);
        expect($('span[data-'+order.siteKey+'-tst="of-ppr_dtls_errr"]').getText()).is.equal('Please enter your paper details');
    });

    it("should goto Order Form 2nd step", function(){
        order.goToOrderForm();
        $('textarea[data-'+order.siteKey+'-tst="of-ppr_dtls"]').scrollIntoView(); //scroll to make Continue button visible  
        order.fillPaperDetails();
        order.goToCheckout();
        const orderTitle = $("(//div[@class = 'order-title'])[2]");//declare OF title at 2nd step 
        browser.pause(500);
        expect(orderTitle.getText()).is.equal('Complete your order');
        
    })

    it("should make order as existing client with upgrade popup by credit card", function(){
        
        order.goToOrderForm();
        browser.pause(500);
        order.setAcademicLvl('401'); // 401 = 4 is site code; 01 is academic level code, "Undergraduate" in our case
        $('textarea[data-'+order.siteKey+'-tst="of-ppr_dtls"]').scrollIntoView(); //scroll to make Continue button visible  
        order.fillPaperDetails();
        order.goToCheckout();
        order.asExistingClient(); //choose 'I have an account' option
        clientAuth.typeExistingClientEmailOF('test2@test.test', order.siteKey);
        clientAuth.typeExistingClientPassOF('123123', order.siteKey);
        order.payByCard();
        //When Upgrade popup is visible
        order.closeUpgradePopup();
        //When Upgrade popup is hidden
        browser.pause(9000);
        expect(browser.getTitle()).to.be.equal('Secure Order Payment');//verify title for complience
    })

    it("should make order as new client with upgrade popup by credit card", function(){
        order.goToOrderForm();
        order.setAcademicLvl('401');
        $('textarea[data-'+order.siteKey+'-tst="of-ppr_dtls"]').scrollIntoView(); //scroll to make Continue button visible  
        order.fillPaperDetails();
        order.goToCheckout();
        order.asNewClient();
        clientAuth.typeNewClientEmailOF();
        order.allowGdprAgree();
        order.payByCard();
        //When Upgrade popup is visible
        order.closeUpgradePopup();
        //When Upgrade popup is hidden
        browser.pause(9000);
        expect(browser.getTitle()).to.be.equal('Secure Order Payment');//verify title for complience
    })

})