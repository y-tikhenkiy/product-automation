import { AuthForm } from "../../page_object/authForm";
import { expect } from "chai";
import faker = require("faker");
import { ClientPanel } from "../../page_object/clientPanel";

describe('login form', function(){
    before(function(){
        browser.maximizeWindow();
    })
    
    beforeEach(function(){
        browser.url('./');
        browser.pause(500);
        browser.deleteCookies();
            
        //browser.click('#hide-cookie-banner'); //hide GDPR banner
    })

    let clientAuth: AuthForm = new AuthForm('ew');
    let clientPanel: ClientPanel = new ClientPanel('ew');
    
    it('should go to login form', function(){
        clientAuth.goToAuth();
        expect(browser.getTitle()).to.be.equal('Auth');//verify title for complience
    });

    it('should sign in as registered account', function(){
        clientAuth.goToAuth();
        clientAuth.typeRegisteredEmailAuthForm('test2@test.test');
        clientAuth.typeRegisteredPassAuthForm('123123');
        clientAuth.signinBtnClick(); // click to sign in client panel as authorized client
        browser.pause(5000);
        expect($(clientPanel.signOutBtn).isDisplayed()).to.be.true;

        clientPanel.signOutBtnClick();

    })

    it('should go to registration form', function(){
        clientAuth.goToAuth();
        clientAuth.goToRegistrForm();

        expect(browser.getTitle()).to.be.equal('Registration');//verify title for complience
    })

    it('should register new account with registration form', function(){
        clientAuth.goToAuth();
        clientAuth.goToRegistrForm();
        clientAuth.typeFirstName();
        clientAuth.typeLastName();
        clientAuth.typeEmailRegistration();
        clientAuth.typePhoneRegistraion();
        clientAuth.gdprClick(); // click at GDPR agreement
        clientAuth.registrationBtnClick(); // click at Register button

        let clntProfileFirstName =$('#header-profile-link');
        clntProfileFirstName.waitForDisplayed(undefined);
        
        expect(clntProfileFirstName.getText()).to.be.equal(clientAuth.firstName);

        clientPanel.signOutBtnClick();
    })

})