import { OFControls } from "../../page_object/orderFormControls";
import { InqControls } from "../../page_object/inqControls";
import { expect } from "chai";
import { ClientPanel } from "../../page_object/clientPanel";

describe('surfing at inquiry', ()=>{
    before(()=>{
        browser.maximizeWindow();
    })
    
    beforeEach(()=>{
        browser.url('./');
        browser.pause(500);
        browser.deleteCookies();
            
        //browser.click('#hide-cookie-banner'); //hide GDPR banner
    })
    //variables
    let inquiry: InqControls = new InqControls('ew'); //initialization of InqControls class object
    let clientPanel: ClientPanel = new ClientPanel('ew');

    it('should go to inquiry page from main page', ()=>{
        inquiry.goToInquiry();
        let inqPageTitle = browser.getTitle();
        expect(inqPageTitle).to.equal('Get a FREE Qoute at Our Custom Writing Service');
    })
    
    it('should make order as existing client',()=>{
        inquiry.goToInquiry();
        inquiry.typeSubject();
        inquiry.typeTopic();
        inquiry.typeDescription();
        inquiry.scrollToDescription();
        inquiry.typeExistingClientEmail();
        inquiry.typeExistingClientPass();
        inquiry.clickSubmitInqBtn();
        
        browser.pause(10000);
        
        
        expect($(clientPanel.signOutBtn).isExisting()).to.be.true;
        clientPanel.signOutBtnClick()
        
    })

    xit('should make order as new client',()=>{
        inquiry.goToInquiry();
        inquiry.typeSubject();
        inquiry.typeTopic();
        inquiry.typeDescription();
        inquiry.scrollToDescription();
        inquiry.asNewClient();
        inquiry.typeNewClientEmail();
        inquiry.typeFirstName('inq');
        inquiry.typeLastName('inq');
        inquiry.typePhone();
        inquiry.gdprClick();
        inquiry.clickSubmitInqBtn();
        
        browser.pause(12000);
        inquiry.skipIntro();
        expect($(clientPanel.signOutBtn).isExisting()).to.be.true;
        clientPanel.signOutBtnClick()
    })
})