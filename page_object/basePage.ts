import faker = require("faker");

class BasePage{
    
    public siteKey: string;
    public readonly firstName = faker.name.firstName();
    public readonly lastName = 'test';

    constructor(key: string){
        this.siteKey = key;
    }

    genClientEmail(firstName = this.firstName, lastName = this.lastName):any{
       
        return faker.internet.email(firstName, lastName);
    }

    typeFirstName(attributePrefix = 'reg', firstName = this.firstName): any{ //if it attributePrefix is default, its for inquiry first name input
        const firstNameInput = $('[data-'+this.siteKey+'-tst='+attributePrefix+'-frst_name]');
        firstNameInput.waitForDisplayed(5000);
        firstNameInput.setValue(firstName);
    }

    typeLastName(attributePrefix = 'reg', lastName = this.lastName): any{
        const lastNameInput = $('[data-'+this.siteKey+'-tst='+attributePrefix+'-lst_name]');
        lastNameInput.waitForDisplayed(5000);
        lastNameInput.setValue(lastName);
    }

} 
export{BasePage}
