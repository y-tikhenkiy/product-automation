import faker = require("faker");
import { OFControls } from "./orderFormControls";
import { BasePage } from "./basePage";


class AuthForm extends BasePage{
    
       
    
    
    goToAuth() {
        const signinBtn = $('[data-'+this.siteKey+'-tst="hdr-sgn_in"');
        signinBtn.waitForDisplayed(5000);
        signinBtn.click();
    }

    goToRegistrForm() :any {
        const registrNow = $('[data-'+this.siteKey+'-tst="auth-rgstr-btn"');
        registrNow.waitForDisplayed(5000);
        registrNow.click();
    }

    gdprClick() {
        const btnAttribute = $('[data-'+this.siteKey+'-tst=reg-agree-gdpr]');
        btnAttribute.waitForDisplayed(5000);
        btnAttribute.click();

    }

    signinBtnClick() {
        const btnAttribute = $('[data-'+this.siteKey+'-tst=auth-sbmt-btn]');
        btnAttribute.waitForDisplayed(5000);
        btnAttribute.click();
    }
    registrationBtnClick() {
        const btnAttribute = $('[data-'+this.siteKey+'-tst=reg-rgstr-btn]');
        btnAttribute.waitForDisplayed(5000);
        btnAttribute.click();

    }

    //will type email for ordering essay & registration as new client
    typeNewClientEmailOF(): any{
        const emailInput = $('[data-'+this.siteKey+'-tst=of-eml-clnt]');
        emailInput.waitForDisplayed(5000);
        emailInput.setValue(this.genClientEmail(this.firstName, this.lastName));
    }
    

    typeExistingClientEmailOF(email: string, siteKey: string): any{
        const emailInput = $('input[data-'+siteKey+'-tst=of-eml-clnt]');
        emailInput.waitForDisplayed(5000);
        emailInput.setValue(email);
    }
    typeExistingClientPassOF(pass:string, siteKey:string): any{
        const passInput = $('[data-'+siteKey+'-tst=of-pass-old_clnt]');
        passInput.waitForDisplayed(5000);
        passInput.setValue(pass);
    }
    typeRegisteredEmailAuthForm(email: string): any {
        const emailInput = $('[data-'+this.siteKey+'-tst=auth-eml]');
        emailInput.waitForDisplayed(5000);
        emailInput.setValue(email);
     
    }

    typeRegisteredPassAuthForm(pass: string): any{
        const passInput = $('[data-'+this.siteKey+'-tst=auth-pass]');
        passInput.waitForDisplayed(5000);
        passInput.setValue(pass);
    }

    typeEmailRegistration(): any{
        const emailInput = $('[data-'+this.siteKey+'-tst=reg-email]');
        emailInput.waitForDisplayed(5000);
        emailInput.setValue(this.genClientEmail(this.firstName, this.lastName));
    }
    
    typePhoneRegistraion(phoneNumber = '123123123'): any {
        const phoneNumberInput = $('[data-'+this.siteKey+'-tst=reg-phn]');
        phoneNumberInput.waitForDisplayed(5000);
        phoneNumberInput.setValue(phoneNumber);        
    }

    /*typePhone(phoneNumber: string): any{
        const phoneNumberInput = $('input[name="phone_number"]');
        phoneNumberInput.waitForDisplayed(5000);
        phoneNumberInput.setValue(phoneNumber);
    }
*/
   
}

export {AuthForm}