import { BasePage } from "./basePage";

class ClientPanel extends BasePage{
    public readonly signOutBtn = '[data-'+this.siteKey+'-tst="adm-sgn_out-btn"]';
    
    signOutBtnClick(): any{
        // const signOutBtn = $('[data-'+this.siteKey+'-tst="adm-sgn_out-btn"]');
        $(this.signOutBtn).waitForDisplayed(5000);
        $(this.signOutBtn).click();
    }
}

export{ClientPanel}