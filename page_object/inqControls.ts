import { BasePage } from "./basePage";

class InqControls extends BasePage{
    
    
       
    // public readonly siteKey: string;

    // constructor(siteKey: string){
    //     this.siteKey = siteKey;
    // }    

    goToInquiry(): any{
        $('[data-'+this.siteKey+'-tst="hdr-inqr"]').click(); // clicking at Order Now button at main page by XPath
        browser.pause(500);
    };

    asNewClient() {
        const iAmNewHereTab =$('[data-'+this.siteKey+'-tst="inq-chs_old_clnt"]');
        iAmNewHereTab.waitForDisplayed(5000);
        iAmNewHereTab.click();

    }

    typeSubject(): any{ //should b rewrite selector in according with Viktor requirements (data attribute)
        const subject = $('#subject_test');
        subject.setValue('Accounting');
    }

    typeTopic(): any{
        const topic = $('[data-'+this.siteKey+'-tst="inq-tpc"]');  //data-ew-tst="inq-tpc"
        topic.setValue('test');
    }

    typeDescription(): any {
        const description = $('[data-'+this.siteKey+'-tst="inq-ppr_dtls"]');
        description.setValue('test test testbyit');
    }

    ////////////////////Emails 
    typeExistingClientEmail(): any {
        const emailInput = $('[data-'+this.siteKey+'-tst="inq-eml-old_clnt"]');
        emailInput.waitForDisplayed(5000);
        emailInput.setValue('test2@test.test');
    }
    typeNewClientEmail(): any{
        const emailInput = $('[data-'+this.siteKey+'-tst="inq-eml-new_clnt"]');
        emailInput.waitForDisplayed(5000);
        emailInput.setValue(this.genClientEmail(this.firstName, this.lastName));
    }
    ////////////////////////////

    typePhone(phoneNumber = '123123123'): any {
        const phoneNumberInput = $('[data-'+this.siteKey+'-tst=inq-phn]');
        phoneNumberInput.waitForDisplayed(5000);
        phoneNumberInput.setValue(phoneNumber);      
    }
    

    typeExistingClientPass(): any {
        const passInput = $('[data-'+this.siteKey+'-tst="inq-pass-old_clnt"]');
        passInput.waitForDisplayed(5000);
        passInput.setValue('123123');
    }

    gdprClick(): any{
        const gdprAgreeSelector = $('[data-'+this.siteKey+'-tst="inq-agree-gdpr"]');
        gdprAgreeSelector.waitForDisplayed(5000);
        gdprAgreeSelector.moveTo(1,1); //Move the mouse to the x=1 y=1 coordinates of the GDPR checkbox.
        browser.positionClick(0); //Left clicks at the current mouse coordinates (set by moveto).
    }

    clickSubmitInqBtn() {
        const submitInqBtn = $('[data-'+this.siteKey+'-tst="inq-sbmt-btn"]');
        submitInqBtn.waitForDisplayed(5000);
        submitInqBtn.click();
    }
    scrollToDescription() {
        const descriptionInput = $('[data-'+this.siteKey+'-tst="inq-ppr_dtls"]');
        descriptionInput.waitForDisplayed(5000);
        descriptionInput.scrollIntoView();
    }


    // data-ew-tst="adm-introjs-skipbutton"

    skipIntro(): any{
        const skipBtn = $('[data-'+this.siteKey+'-tst="adm-introjs-skipbutton"]');
        skipBtn.waitForDisplayed(5000);
        skipBtn.click();
        browser.pause(500);
    }
}

export {InqControls};