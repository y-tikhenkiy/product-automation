import { ifError } from "assert";

class OFControls{
    

    public readonly siteKey: string;
    public readonly chcktBtn: string; 

    constructor(siteKey: string){
        this.siteKey = siteKey;
        this.chcktBtn = 'button[data-'+this.siteKey+'-tst="of-checkt-btn"]'; 
    }

    goToOrderForm(): any{
        const orderNowBtn = $('button[data-'+this.siteKey+'-tst="hdr-ordr_now"]'); 
        orderNowBtn.click(); // clicking at Order Now button at main page
        browser.pause(500);
    };
    
    fillPaperDetails(): any{
        const paperDetailsInput = $('textarea[data-'+this.siteKey+'-tst="of-ppr_dtls"]')
        paperDetailsInput.setValue('test test testbyit');
    };

    closeUpgradePopup(): any{
        const  noThnxBtn = $('a[data-'+this.siteKey+'-tst="no-thnx"]');
        noThnxBtn.waitForDisplayed(5000);
        noThnxBtn.click();
    }

    setAcademicLvl(acadLvlID:string):any{
        const academicLvlSelector = 'select[data-'+this.siteKey+'-tst="of-acdm_lvl"]';
        $(academicLvlSelector).waitForDisplayed(5000);
        $(academicLvlSelector).selectByAttribute('value', acadLvlID);
    }

    goToCheckout(): any{
        const checkoutBtn = $(this.chcktBtn);
        checkoutBtn.scrollIntoView();
        checkoutBtn.waitForDisplayed(5000);
        checkoutBtn.click();
    }

    asExistingClient(): any{
        const existingClientSelector = 'label[data-'+this.siteKey+'-tst="of-chs_old_clnt"]';
        $(existingClientSelector).waitForDisplayed(5000);
        $(existingClientSelector).click();
    }
    asNewClient(): any{
        const newClientSelector ='label[data-'+this.siteKey+'-tst="of-chs_rgstr_clnt"]';
        $(newClientSelector).waitForDisplayed(5000);
        $(newClientSelector).click();
    }
    
    payByCard(): any{
        const btnPayByCard = $('button[data-'+this.siteKey+'-tst="pay_by_crd-btn"]');
        btnPayByCard.scrollIntoView();
        btnPayByCard.waitForDisplayed(5000);
        btnPayByCard.click();
    }

    allowGdprAgree(): any{
        const gdprAgreeSelector = $('[data-'+this.siteKey+'-tst="of-agree-gdpr"]');
        gdprAgreeSelector.waitForDisplayed(5000);
        gdprAgreeSelector.click();       
    }

    /*getSiteKey():any{
        return this.siteKey;      
    }*/
}

export {OFControls};